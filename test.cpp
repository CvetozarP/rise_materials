#include <iostream>
#include "textNode.hpp"

int main(){
TextNode node("url");
node.setText("TEST\nTEST");

if(node.getLineCount() == 0){

    std::cout << "Failed" << std::endl;
    return 1;
} else {
std::cout << "Success " << std::endl;
return 0;
}

}
