#include <iostream>
#include <string>
#include "LibraryItem.h"

class Magazine : public LibraryItem{
    private:
        int issuedNum;
    public:
        Magazine();
        Magazine(int issued , string title, int year);
        void display() override;

};