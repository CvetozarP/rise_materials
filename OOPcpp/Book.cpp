#include "Book.h"

void Book::setName(string n){
    authorName = n;
}
string Book::getName(){
    return authorName;
}
Book::Book(): LibraryItem(){}
Book::Book(string name, string title, int year) : LibraryItem(title, year),authorName(name){}
void Book::display(){
     cout << "Author name: " << authorName << " Item name: " << getTitle() <<  " and year: " << getYear() << endl;
}
