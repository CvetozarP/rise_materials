#!/bin/sh
set -e

# Install location
PREFIX=/usr/local

# Compile lib
options="-std=c++17 -Ofast -flto -march=native -DNDEBUG -Wall -Wextra -Wno-parentheses -Wno-unused-result -s -pipe"
# options="-std=c++17 -g"
g++ -c $options Book.cpp LibraryItem.cpp Magazine.cpp

# Create lib
ar rc libnew.a Book.o LibraryItem.o Magazine.o

# Compile cppipe
g++ -o new $options task1.cpp -lnew -L.
chmod 755 new

# library is embedded in the new command
