#include <iostream>
#include <string>
#include "LibraryItem.h"

class Book : public LibraryItem{
    private :
        string authorName;

    public : 
        void setName(string n);
        string getName(); 
        Book();
        Book(string name, string title, int year);
        void display() override;

};
