#pragma once
#include <iostream>
#include <string>

using namespace std;
class LibraryItem{

    private:
        string title;
        int year;
       
    public : 
        static int countItems;
        void setTitle(string title);
        string getTitle();
        void setYear(int y);
        int getYear(); 
        LibraryItem();
        static int getCount();
        LibraryItem(string title, int year);
        LibraryItem(const LibraryItem& lItem);
        virtual ~LibraryItem();
        virtual void display();

};