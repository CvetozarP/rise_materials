#include "LibraryItem.h"

void LibraryItem::setTitle(string t){
    title = t;
}
string LibraryItem::getTitle(){
    return title;
}
void LibraryItem::setYear(int y){
    year = y;
}
int LibraryItem::getYear(){
    return year;
}
LibraryItem::LibraryItem(){
    countItems++;
}
int LibraryItem::getCount(){
    return countItems;
}

LibraryItem::LibraryItem(string t, int y){
    title = t;
    year = y;
    countItems++;
}

LibraryItem::LibraryItem(const LibraryItem& lItem) : title(lItem.title), year(lItem.year){
    countItems++;
}
LibraryItem:: ~LibraryItem(){
    countItems--;
}

void LibraryItem::display(){
     cout << "Item name: " << title <<  "and year: " << year << endl;
}

int LibraryItem :: countItems = 0;