#include <gmock/gmock.h>
#include "textNode.hpp"
#include "node_mock.hpp"
#include "view.hpp"


TEST(Tests, getLineCount)
{
	TextNode node("/MainDisplay/TMPS_Menu");
	node.setText("TEST\nTEST\n");

	// if(node.getLineCount() == 0){
	// 	std::cout << "Failed" << std::endl;
	// 	//return 1;
	// 	//ERR
	// } else {
	// std::cout << "Success " << std::endl;
	// //return 0;
	// }
	EXPECT_EQ(node.getLineCount(), 2);//
	ASSERT_EQ(node.getLineCount(), 1);
}

//testing void func
// TEST(Tests, changeVisibility){
// 	TextNode node("/MainDisplay/TMPS_Menu");
// 	bool visible = true;
// 	node.changeVisibility(visible);
	
// 	//bool yes = node.changeVisibility(visible);
// 	//todo
// 	EXPECT_EQ(visible, true);
	

// }

class TestTextNode: public TextNode{
	public :
		using TextNode::TextNode;
		using TextNode::text_;

};
TEST(View, render){
	View view;
	NodeMock* node = new NodeMock("/URL");
	view.addNode(node);

	EXPECT_CALL(*node, draw()).Times(1);
	view.render();


}

TEST(View, show){
	View view;
	NodeMock* node = new NodeMock("/URL");
	view.addNode(node);
	bool visible = true;
	node->changeVisibility(visible);
	
	EXPECT_CALL(*node, changeVisibility(visible)).Times(1);
	view.show();
}

TEST(View, hide){
	View view;
	NodeMock* node = new NodeMock("/URL");
	view.addNode(node);
	bool visible = false;
	node->changeVisibility(visible);
	
	EXPECT_CALL(*node, changeVisibility(false)).Times(1);
	view.hide();
}


TEST(Tests, setLineCount){
	TestTextNode node("url");
	node.setText("LINE\nLine");
	node.setLineCount(1);
	int count = 0;
	for(char c : node.text_){
		if('\n' == c){
			++count;
		}
	}

	EXPECT_EQ(count, 1);

}


int main(int argc, char* argv[])
{
	// testing::InitGoogleTest();
	testing::InitGoogleMock();
	return RUN_ALL_TESTS();
}
